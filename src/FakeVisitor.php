<?php
namespace FakeVisitor;

use Framework\Network\Curl\Curl;

class FakeVisitor extends Curl
{

    protected $browser;

    public function __construct(FakeBrowser $browser)
    {
        $this->browser = $browser;

        parent::__construct();

        $this->timeout()
            ->returnValue(true)
            ->referer($this->browser->getReferer())
            ->userAgent($this->browser->getUserAgent());
    }

    public function setCookie($cookie)
    {
        $this->cookie($cookie);

        return $this;
    }

    public function visit(string $url)
    {
        return $this
                ->setUrl($url)
                ->output();
    }
}
